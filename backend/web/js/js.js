window.onload=function(){
    var csrfParam = $('meta[name="csrf-param"]').attr("content");
    var csrfToken = $('meta[name="csrf-token"]').attr("content");
    var pdf_id = $('#pdf').data('id');

    $.ajax({
        url: '/users/pdf/get-pdf-description-info/',
        type: 'post',
        data: {'id':pdf_id},
        success: function (response) {
            if(response.success == true){
                setValues(response.data);
            }
        }
    });

    function setValues(data){
        $.each(data, function(key, value){
            if(typeof value == 'object'){
                if(key == 'lessor_info'){
                    for(var i = 1; i < value.length; i++){
                        $('#add_another_lessor').trigger('click');
                    }
                }
                if(key == 'lessee_info'){
                    for(var i = 1; i < value.length; i++){
                        $('#add_another_lessee').trigger('click');
                    }
                }
                if(key == 'my_appliances_options'){
                    for(key in value){
                        $('#add_my_option_appliances').val(key);
                        $('#add_my_option_appliances').trigger('keyup', true);
                    }
                } else {
                    $.each(value, function(subkey, subvalue){
                        $.each(subvalue, function(sub_subkey, sub_subvalue){
                            var elem = $('*[name=\"'+key+'['+subkey+']'+'['+sub_subkey+']'+'\"]');
                            setValueInElem(elem, sub_subkey, sub_subvalue);
                        });
                    });
                }
            } else {
                if(key == 'rent_pay_period' && value == 'week'){
                    $('#rent_pay_period').val('week').trigger('change');
                }
                var elem = $('*[name='+key+']');
                if($(elem).attr('type') == 'radio'){
                    elem = $('*[name='+key+'][value="' + value + '"]');
                }
                setValueInElem(elem, key, value);
            }
        });
    }

    function setValueInElem(elem, key, value){
        if($(elem).attr('type') == 'text' || $(elem).attr('type') == 'select'){
            $(elem).val(value);
        }

        if($(elem).attr('type') == 'checkbox' || $(elem).attr('type') == 'radio' ){
            $(elem).iCheck('check')
        }

        //if(($(elem).attr('type') == 'select')){
        //    $(elem).iCheck('check')
        //}
    }
};

