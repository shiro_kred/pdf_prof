<?php

use yii\db\Migration;
use yii\db\Schema;

class m160324_044417_create_table_from_pdf extends Migration
{
    public function up()
    {
        $this->createTable('pdf_form_rent', [
            'id' => $this->primaryKey(),
            'name' => Schema::TYPE_STRING . '(64) NOT NULL'
        ]);

        $this->batchInsert('pdf_form_rent', ['name'], [
            ['Single Family House'],
            ['Apartament'],
            ['Condo'],
            ['Room'],
            ['Portion of Duplex or Row Housing'],
            ['Mobile Home'],
            ['Mobile Home Site']
        ]);
    }

    public function down()
    {
        $this->dropTable('pdf_form_rent');
    }
}
