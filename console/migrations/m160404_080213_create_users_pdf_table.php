<?php

use yii\db\Migration;
use yii\db\Schema;

class m160404_080213_create_users_pdf_table extends Migration
{
    public function up()
    {
        $this->createTable('users_pdf', [
            'id' => $this->primaryKey(),
            'user_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'description' => Schema::TYPE_TEXT . ' NOT NULL',
            'created_at' => Schema::TYPE_INTEGER . ' NOT NULL',
            'updated_at' => Schema::TYPE_INTEGER . ' NOT NULL',
            'status_paid' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT \'0\'',
            'transaction' => Schema::TYPE_STRING . '(53) DEFAULT \'0\'',
            'type' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT \'0\'',
        ]);
    }

    public function down()
    {
        $this->dropTable('users_pdf');
    }
}
