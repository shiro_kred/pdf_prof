<?php

use yii\db\Migration;
use yii\db\Schema;

class m160413_083357_users_address extends Migration
{
    public function up()
    {
        $this->createTable('users_address', [
            'id' => $this->primaryKey(),
            'user_id' => Schema::TYPE_INTEGER,
            'name' => Schema::TYPE_STRING,
            'type' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT \'0\'',
            'phone' => Schema::TYPE_STRING,
            'street' => Schema::TYPE_STRING,
            'apt' => Schema::TYPE_STRING,
            'municipality' => Schema::TYPE_STRING,
            'province' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT \'0\'',
            'postal_code' => Schema::TYPE_STRING,
            'created_at' => Schema::TYPE_INTEGER,
            'updated_at' => Schema::TYPE_INTEGER,
            'lessor' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT \'0\'',
        ]);

        $this->createIndex('user_id', 'users_address', 'user_id', true);
    }

    public function down()
    {
        $this->dropTable('users_address');
        echo "m160413_083357_users_address cannot be reverted.\n";
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
