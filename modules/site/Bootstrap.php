<?php

namespace root\modules\site;

use yii\base\BootstrapInterface;

/**
 * Gallery module bootstrap class.
 */
class Bootstrap implements BootstrapInterface
{
    /**
     * @inheritdoc
     */
    public function bootstrap($app)
    {
        // Add module URL rules.
        $app->getUrlManager()->addRules(
            [
                '' => 'site/default/index',
                '<_a:(about|contacts|captcha)>' => 'site/default/<_a>'
            ]
        );

        // Add module I18N category.
        if (!isset($app->i18n->translations['root/modules/site']) && !isset($app->i18n->translations['root/modules/*'])) {
            $app->i18n->translations['root/modules/site'] = [
                'class' => 'yii\i18n\PhpMessageSource',
                'basePath' => '@root/modules/site/messages',
                'forceTranslation' => true,
                'fileMap' => [
                    'root/modules/site' => 'site.php',
                ]
            ];
        }
    }
}
