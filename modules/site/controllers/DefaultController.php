<?php

namespace root\modules\site\controllers;

use root\modules\site\components\Controller;
use root\modules\site\models\ContactForm;
use root\modules\users\models\LoginForm;
use root\modules\users\models\Pdf;
use root\modules\users\models\frontend\User;
use root\modules\users\models\Profile;
use root\modules\site\Module;
use yii\captcha\CaptchaAction;
use yii\web\ErrorAction;
use yii\web\ViewAction;
use Yii;

/**
 * Frontend main controller.
 */
class DefaultController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => ErrorAction::className()
            ],
            'about' => [
                'class' => ViewAction::className(),
                'defaultView' => 'about'
            ],
            'captcha' => [
                'class' => CaptchaAction::className(),
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
                'backColor' => 0XF5F5F5,
                'height' => 34
            ]
        ];
    }

    /**
     * Site "Home" page.
     */
    public function actionIndex()
    {
        $this->layout = '//home';
        $model = new LoginForm();

        $user = new User(['scenario' => 'signup']);
        $profile = new Profile();

        return $this->render('index',
            [
                'model' => $model,
                'user' => $user,
                'profile' => $profile
            ]
        );
    }

    /**
     * Site "Contact" page.
     */
    public function actionContacts()
    {
        $model = new ContactForm;
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash(
                'success',
                Module::t('site', 'CONTACTS_FLASH_SUCCESS_MSG')
            );
            return $this->refresh();
        } else {
            return $this->render(
                'contacts',
                [
                    'model' => $model
                ]
            );
        }
    }

    public function actionCreatePdf(){
        $model = new LoginForm();
        $user = new User(['scenario' => 'signup']);
        $profile = new Profile();

        return $this->render('create-pdf', [
            'model' => $model,
            'user' => $user,
            'profile' => $profile
        ]);
    }
}
