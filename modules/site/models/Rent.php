<?php

namespace root\modules\site\models;

use root\modules\users\traits\ModuleTrait;
use yii\db\ActiveRecord;
use Yii;

/**
 * Class Profile
 * @package root\modules\site\models
 * Rent profile model.
 *
 */
class Rent extends ActiveRecord
{
    use ModuleTrait;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
//        return '{{%profiles}}';
        return 'pdf_form_rent';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Name',
        ];
    }

    public static function getlistInArray(){
        $listRent = self::find()->asArray()->all();
        $result = [];
        foreach($listRent as $value){
            $result[$value['id']] = $value['name'];
        }

        return $result;
    }
}
