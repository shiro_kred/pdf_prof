<?php
use yii\helpers\Url;
?>

    <div class="content_popup_signin" id="window_signin">
        <div class="header_signin">
            <span>Sign Up</span>
            <img src="<?= $this->assetManager->publish('@root/modules/themes/site/assets/images/first_page/signuo-grey-rectangle.png')[1] ?>" class="img_header_signin">
        </div>
        <?= $this->render('form/_sign_form', ['profile' => $profile, 'user' => $user]) ?>
        <div class="login_sub_information">
            <a href="#" id="or_login">or Login</a>
        </div>
    </div>
    <div id="overlay"></div>
    <div id="window_login">
        <img src="<?= $this->assetManager->publish('@root/modules/themes/site/assets/images/first_page/Loginback.png')[1] ?>">
        <div class="header_login">Login</div>
        <?= $this->render('form/_login_form', ['model' => $model]) ?>
        <div class="login_sub_information">
            <a href="#" id="or_sign_up">or Sign Up</a>
        </div>
    </div>

    <div class="second_menu">
        <h1 class="text_second_menu">Rental/lease Agreement</h1>
        <div class="container-fluid">
            <div class="load_second_menu">
                <div class="white_load" id="line_load"></div>
            </div>
        </div>
    </div>
    <div class="container-fluid container-form" id="create_page">
        <div class="page form-group-lg">
            <div class="page_1">
                <?= $this->render('pdf_form/form_page_1') ?>
            </div>
            <div class="page_2" style="display: none;">
                <?= $this->render('pdf_form/form_page_2') ?>
            </div>
            <div class="page_3" style="display: none;">
                <?= $this->render('pdf_form/form_page_3') ?>
            </div>
            <div class="page_4" style="display: none;">
                <?= $this->render('pdf_form/form_page_4') ?>
            </div>
            <div class="page_5" style="display: none;">
                <?= $this->render('pdf_form/form_page_5') ?>
            </div>
            <div class="page_6" style="display: none;">
                <?= $this->render('pdf_form/form_page_6') ?>
            </div>
            <div class="page_7" style="display: none;">
                <?= $this->render('pdf_form/form_page_7') ?>
            </div>
            <div class="page_8" style="display: none;">
                <?= $this->render('pdf_form/form_page_8') ?>
            </div>
            <div class="page_9" style="display: none;">
                <?= $this->render('pdf_form/form_page_9') ?>
            </div>
            <div class="page_10" style="display: none;">
                <?= $this->render('pdf_form/form_page_10') ?>
            </div>
            <div class="page_11" style="display: none;">
                <?= $this->render('pdf_form/form_page_11') ?>
            </div>
            <div class="page_12" style="display: none;">
                <?= $this->render('pdf_form/form_page_12') ?>
            </div>
            <div class="col-xs-4 col-md-11 col-sm-11 container-butt">
                <div class="multiform_button_block">
                    <button type="button" class="btn btn-default back-button btn-lg" id="button_back">BACK</button>
                    <button type="button" class="btn btn-default continue-button btn-lg" id="button_continue">CONTINUE</button>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid" id="result_page" style="display: none">
        <h1 class="text_second_menu_2">Excellent! Your Rental/Lease Agreement is ready. Congratulations!
            You can either download or save your work.</h1>
        <div class="container-fluid" id="buttons_second_menu">
            <button type="button" class="btn btn-primary btn-lg" id="back_second_menu">Go back to editing</button>
            <button type="button" class="btn btn-primary btn-lg" id="save_second_menu">Save your work</button>
            <button type="button" class="btn btn-primary btn-lg" id="download_second_menu">Download</button>
        </div>
        <div class="container-fluid  container_2" id="main">
            <div class="row">
                <div class="container-fluid" id="pdf_main">
                    <img src="<?= $this->assetManager->publish('@root/modules/themes/site/assets/images/Example.png')[1] ?>">
                </div>
                <img src="<?= $this->assetManager->publish('@root/modules/themes/site/assets/images/scrollback.png')[1] ?>" class="scrollback_main">
            </div>
        </div>
    </div>

<?php
$this->registerJs("
        var page_number = 1;

        $('#button_back').on('click', function(){
            if(page_number < 2){
                return false;
            }
            scrollTop();
            showPreviousPage();
        });

        $('#button_continue').on('click', function(){
            scrollTop();
            if(page_number + 1 > 12){
                $('#create_page').hide('slow');
                $('#result_page').show('slow');
            }
            showNextPage();
        });

        $('#back_second_menu').on('click', function(){
            showPreviousPage(false);
            $('#create_page').show('slow');
            $('#result_page').hide('slow');
        });

        $(function () {
            $('#datetimepicker_start input').datepicker({});
            $('#datetimepicker_end input').datepicker({});
        });

        function showNextPage(effect = 'slow'){
            $('.page_'+page_number).hide(effect);
            page_number++;
            $('.page_'+page_number).show(effect);
            changeLineLoad(page_number);
        };

        function showPreviousPage(effect = 'slow'){
            $('.page_'+page_number).hide(effect);
            page_number--;
            $('.page_'+page_number).show(effect);
            changeLineLoad(page_number);
        };

        function scrollTop(){
            $('body').animate({'scrollTop':80},'slow');
        }

        function changeLineLoad(size){
            $('#line_load').animate({
                width: (100/12 * (size - 1)) + '%'
            }, 1000);
        }

        $('#pdf_main').niceScroll({cursorcolor:'#f6f5f5',cursorwidth:11,cursoropacitymin:1,railpadding:{right:-20} });

    ", yii\web\View::POS_READY);
?>