<?php
use root\modules\users\Module;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
?>

<?php $form = ActiveForm::begin([
        'layout' => 'horizontal',
        'action' => Url::toRoute(['/login']),
        'id' => 'form_login',
        'class' => 'form-horizontal',
        'fieldConfig' => [
            'horizontalCssClasses' => [
                'label' => 'col-sm-2',
                'wrapper' => 'col-sm-10',
                'error' => '',
                'hint' => '',
            ],
        ],
    ]
); ?>
        <?= $form->field($model, 'email')->input('email', ['placeholder' => $model->getAttributeLabel('email')]) ?>
        <?= $form->field($model, 'password')->passwordInput(['placeholder' => $model->getAttributeLabel('password')]) ?>
        <?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'id'=>"button_login"]) ?>

        &nbsp;
        <?= Html::a('Forgot Password?', ['recovery'], [ 'id' => 'login_forgot_password']) ?>
<?php ActiveForm::end(); ?>