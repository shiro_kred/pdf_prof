<?php
use root\modules\users\Module;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
?>

    <?php $form = ActiveForm::begin([
            'layout' => 'horizontal',
            'action' => Url::toRoute(['/signup']),
            'id' => 'form_signin',
            'fieldConfig' => [
                'horizontalCssClasses' => [
                    'label' => 'col-sm-2',
                    'wrapper' => 'col-sm-8 slide_input',
                    'error' => '',
                    'hint' => '',
                ],
            ],
        ]
    ); ?>

    <?= $form->field($profile, 'name')->textInput(
        ['placeholder' => $profile->getAttributeLabel('name'), 'id' => 'inputname'])->label('*Name')
    ?>
    <?= $form->field($user, 'email')->textInput(
        ['placeholder' => $user->getAttributeLabel('email'), 'id' => 'inputEmail'])->label('*Email')
    ?>
    <?= $form->field($user, 'password', [
            'horizontalCssClasses' => [
                'wrapper' => 'col-sm-6 slide_input',
            ]
        ])->passwordInput(['placeholder' => $user->getAttributeLabel('password')])->label('*Password')
    ?>
    <?= $form->field($user, 'repassword', [
            'horizontalCssClasses' => [
                'wrapper' => 'col-sm-6 slide_input',
            ]
        ])->passwordInput(['placeholder' => 'Repeat Password'])->label('*Repeat password')
    ?>

    <div class="row">
        <div class="col-md-1 col-md-offset-9">
            <a href="#" class="question_signup"><img src="<?= $this->assetManager->publish('@root/modules/themes/site/assets/images/first_page/question.png')[1] ?>"></a>
        </div>
    </div>
    <div class="checkbox-sign_content_popup_signin">
        <div class="checkbox" id="checkbox_signin">
            <label>
                <input type="checkbox" id="blankCheckbox" value="option1" aria-label="...">
            </label>
        </div>
        <span>I agree to the <a href="#">Terms of Use</a> and <a href="#">Privacy</a></span>
    </div>

    <?= Html::submitButton(
        Module::t('users', 'FRONTEND_SIGNUP_SUBMIT'),
        [
            'class' => 'btn btn-success btn-large pull-left',
            'id'=>"button_signup",
            'disabled' => 'true',
        ]
    ) ?>
    <a href="#" class="learnmore_signin">Learn more</a>

</fieldset>
<?php ActiveForm::end(); ?>

<?php
    $this->registerJs('
        $("#blankCheckbox").on("change", function(){
            if($(this).is(":checked")){
                document.getElementById("button_signup").disabled = false;
            } else {
                document.getElementById("button_signup").disabled = true;
            }
        });
    ', yii\web\View::POS_READY);
?>
