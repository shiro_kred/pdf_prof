<?php
use yii\helpers\Url;
/**
 * Frontend main page view.
 *
 * @var yii\web\View $this View
 */

$this->title = Yii::$app->name;
$this->params['noTitle'] = true; ?>

<div class="content_popup_signin" id="window_signin">
    <div class="header_signin">
        <span>Sign Up</span>
        <img src="<?= $this->assetManager->publish('@root/modules/themes/site/assets/images/first_page/signuo-grey-rectangle.png')[1] ?>" class="img_header_signin">
    </div>
    <?= $this->render('form/_sign_form', ['profile' => $profile, 'user' => $user]) ?>
</div>

<div id="overlay"></div>

<div id="window_login">
    <img src="<?= $this->assetManager->publish('@root/modules/themes/site/assets/images/first_page/Loginback.png')[1] ?>">
    <div class="header_login">Login</div>
    <?= $this->render('form/_login_form', ['model' => $model]) ?>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-16" id="header_container_menu">
            <span>LEASE AGREEMENT</span>
        </div>
    </div>
</div>
<div class="container-fluid" id="img_main">
    <img src="<?= $this->assetManager->publish('@root/modules/themes/site/assets/images/fon.jpg')[1] ?>">
    <div class="container" id="header-button-container_main">
        <h1 class="first_header_main">GET THE KEYS TO YOUR HOME!</h1>
        <h2 class="second_header_main">Create Your Lease Agreement.</h2>
        <a href="/site/default/create-pdf/" type="button" class="btn btn-primary btn-lg" id="create_main">CREATE</a>
    </div>
</div>
<div class="row" id="center_row"></div>
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-5 col-sm-offset-1" id="advantages">
            <span>Just look at the</span>
            <span>ADVANTAGES</span></div>
        <div class="col-sm-5 col-sm-offset-7"  id="first">
            <img src="<?= $this->assetManager->publish('@root/modules/themes/site/assets/images/first_page/1.png')[1] ?>">
            <h1>Save your time.</h1>
            <span>Easy to customize, print and download.</span>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4 col-sm-offset-7" id="second">
            <img src="<?= $this->assetManager->publish('@root/modules/themes/site/assets/images/first_page/2.png')[1] ?>">
            <h1>Easy to create.</h1>
            <span>All you need to do is to enter the data into appropriate fields.</span>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-5 col-sm-offset-7" id="third">
            <img src="<?= $this->assetManager->publish('@root/modules/themes/site/assets/images/first_page/3.png')[1] ?>">
            <h1>Store in cloud.</h1>
            <span>If you register, you get the ability to save documents in a personal archive. You will have access to your documents at any time through any devices.</span>
        </div>
    </div>
    <div class="container-fluid" id="bottom">
        <div class="row" id="bottom">
            <div class= "col-xs-12 col-md-5" id="blue-rectangle">
                <img src="<?= $this->assetManager->publish('@root/modules/themes/site/assets/images/first_page/base.png')[1] ?>">
                <div class="row" id="header_bottom">
                    <div class="col-xs-1 col-md-7" id="header_blue-rectangle">
                        <h1>CREATE YOUR
                            LEASE AGREEMENT
                            in minutes</h1>
                    </div>
                </div>

            <span>Use our attorney-drafted Lease Agreement form
to finalize the renting out of your house,
apartment, condo, basement or attic.<br>
Create a legally-binding relationship
between landlord and tenant.
            </span>
                <button type="button" class="btn btn-primary btn-lg" id="create_blue-rectangle">CREATE</button>
            </div>
            <div class="col-xs-12 col-md-7" id="users">
                <h1>What users are saying?</h1>
                <img src="<?= $this->assetManager->publish('@root/modules/themes/site/assets/images/first_page/ramka.png')[1] ?>" class="ramka_users">
                <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</span>
                <img src="<?= $this->assetManager->publish('@root/modules/themes/site/assets/images/first_page/GeorgeClooney.png')[1] ?>" class="users_users">
                <p>George Clooney</p>
            </div>
        </div>
    </div>
