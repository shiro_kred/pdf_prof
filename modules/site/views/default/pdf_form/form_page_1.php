<div class="row">
    <div class="col-md-11">
        <p class="second_text_main">What type of property are you renting?</p>
    </div>
</div>
<form class="form-horizontal multiform">
    <div class="form-group">
        <div class="block_center">
            <div class="radio">
                <label>
                    <input type="radio" name="renting_type_property" value="Single Family House">
                    Single Family House
                </label>
            </div>
            <div class="radio">
                <label>
                    <input type="radio" name="renting_type_property" value="Apartament">
                    Apartament
                </label>
            </div>
            <div class="radio">
                <label>
                    <input type="radio" name="renting_type_property" value="Condo">
                    Condo
                </label>
            </div>
            <div class="radio">
                <label>
                    <input type="radio" name="renting_type_property" value="Room">
                    Room
                </label>
            </div>
            <div class="radio">
                <label>
                    <input type="radio" name="renting_type_property" value="Portion of Duplex or Row Housing">
                    Portion of Duplex or Row Housing
                </label>
            </div>
            <div class="radio">
                <label>
                    <input type="radio" name="renting_type_property" value="Mobile Home">
                    Mobile Home
                </label>
            </div>
            <div class="radio">
                <label>
                    <input type="radio" name="renting_type_property" value="Mobile Home Site">
                    Mobile Home Site
                </label>
            </div>
        </div>
    </div>
</form>