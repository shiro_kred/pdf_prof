<form class="multiform">
    <div class="col-md-11">
        <p class="multiform_title indent_40_0">Is smoking indoors allowed?</p>
    </div>
    <div class="row">
        <div class="form-group input-lg">
            <div class="col-xs-offset-1 col-sm-5 col-sm-offset-4 col-md-4 col-md-offset-3">
                <div class="col-md-1 col-md-offset-5 col-sm-2 col-sm-offset-0 col-xs-1 col-xs-offset-0">
                    <div class="checkbox"><label>
                            <input type="radio" name="smoking_allowed" value="yes">
                            Yes</label>
                    </div>
                </div>
                <div class="col-md-1 col-md-offset-2 col-sm-2 col-sm-offset-2 col-xs-1 col-xs-offset-1">
                    <div class="checkbox"><label>
                            <input type="radio" name="smoking_allowed"  value="no">
                            No</label>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-11">
        <p class="multiform_title indent_40_0">Will pets be allowed  on the property?</p>
    </div>
    <div class="row">
        <div class="form-group input-lg">
            <div class=" col-xs-offset-1 col-sm-5 col-sm-offset-4 col-md-4 col-md-offset-3">
                <div class="col-md-1 col-md-offset-5 col-sm-2 col-sm-offset-0 col-xs-1 col-xs-offset-0">
                    <div class="checkbox"><label>
                            <input type="radio" name="pets_allowed" value="yes">
                            Yes</label>
                    </div>
                </div>
                <div class="col-md-1 col-md-offset-2 col-sm-2 col-sm-offset-2 col-xs-1 col-xs-offset-1">
                    <div class="checkbox"><label>
                            <input type="radio" name="pets_allowed" value="no">
                            No</label>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <br>
    <div class="row">
            <div class="form-group form-group-lg">
                <div class="col-md-3 col-md-offset-4 col-sm-4 col-sm-offset-4">
                    <label for="Specify">Specify:</label>
                    <input type="text" class="form-control" name="specify" placeholder="">
                </div>
            </div>
    </div>
</form>