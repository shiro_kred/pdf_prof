<form class="multiform">
    <div class="col-md-11">
        <p class="multiform_title indent_40_0">Additional terms or conditions of your Rental/Lease agreement?</p>
    </div>
    <div class="row indent_40">
        <div class="form-group input-lg">
            <div class=" col-xs-11 col-xs-offset-0 col-sm-7 col-sm-offset-2 col-md-7 col-md-offset-1 text-center">
                <div class="col-md-2 col-md-offset-6 col-sm-2 col-sm-offset-3 col-xs-1 col-xs-offset-4">
                    <div class="radio"><label>
                            <input type="radio" name="additional_terms_rental" value="yes">
                            Yes</label>
                    </div>
                </div>
                <div class="col-md-1 col-md-offset-0 col-sm-2 col-sm-offset-1 col-xs-1 col-xs-offset-1">
                    <div class="radio"><label>
                            <input type="radio"  name="additional_terms_rental" value="no">
                            No</label>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <br>
        <br>
        <div class="row">
            <div class="col-md-5 col-md-offset-3 col-sm-7 col-sm-offset-2 col-xs-9 col-xs-offset-1 text-area_additional-terms">
                <textarea class="form-control" type="text" style="height: auto" name="additional_terms_rental_text" rows="6"></textarea>
            </div>
        </div>
    </div>
</form>