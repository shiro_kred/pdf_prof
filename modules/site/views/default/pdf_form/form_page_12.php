<form class="multiform">
    <div class="col-md-11">
        <p class="multiform_title indent_40_0">When will the lease be signed?</p>
    </div>
    <div class="row">
        <div class="form-group">
            <div class="col-md-11 indent_20">
                <div class="col-md-3 col-md-offset-5">
                    <div class="radio">
                        <label>
                            <input type="radio" value="" name="lease_signed_date" value="today">Today
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="lease_signed_date" value="this_month">This month
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="lease_signed_date" value="specify_date">Specify date
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <br>
    <br>

    <div class="row">
        <div class='col-sm-5  col-md-offset-4'>
            <div class='col-sm-5 col-md-offset-1'>
                <div class="form-group">
                    <label style="line-height: 44px;">Specify date:</label>
                    <div class='input-group date' id='datetimepicker_specify_date'>
                        <input type='text' name="lease_signed_date_specify" class="form-control" placeholder="     /      /"/>
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>

<?php
$this->registerJs("
        $(function () {
            $('#datetimepicker_specify_date input').datepicker({});
        });
    ", yii\web\View::POS_READY);
?>