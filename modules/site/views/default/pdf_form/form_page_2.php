<div class="row">
    <div class="col-md-11">
        <p class="multiform_title">Where is the property located?</p>
    </div>
</div>

<div class="form-group-lg">
    <form class="form-horizontal multiform">
        <div class="form-group">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-7">
                        <input type="text" class="form-control" name="property_located_address" placeholder="Street Address and Street Name">
                    </div>
                    <div class="col-md-4">
                        <input type="text" class="form-control" name="property_located_apt" placeholder="Apt, Site or Romm #">
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-5">
                        <input type="text" class="form-control" name="property_located_municipality" placeholder="Municipality (or other)">
                    </div>
                    <div class="col-md-2">
                        <input type="text" class="form-control" name="property_located_province" placeholder="Province">
                    </div>
                    <div class="col-md-4">
                        <input type="text" class="form-control" name="property_located_postal_code" placeholder="Postal Code">
                    </div>
                </div>
            </div>
        </div>
    </form>

    <p class="second_text_main">The Superintendent or Property Manager of the residental premises is:</p>

    <form class="form-horizontal multiform">
        <div class="form-group">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-7">
                        <input type="text" class="form-control" name="superintendent_residental_premises_name" placeholder="Name">
                    </div>
                    <div class="col-md-4">
                        <input type="text" class="form-control" name="superintendent_residental_premises_phone" placeholder="Phone No">
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-7">
                        <input type="text" class="form-control" name="superintendent_residental_premises_address" placeholder="Street Address and Street Name">
                    </div>
                    <div class="col-md-4">
                        <input type="text" class="form-control" name="superintendent_residental_premises_apt" placeholder="Apt, Site or Romm #">
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-5">
                        <input type="text" class="form-control" name="superintendent_residental_premises_municipality" placeholder="Municipality (or other)">
                    </div>
                    <div class="col-md-2">
                        <input type="text" class="form-control" name="superintendent_residental_premises_province" placeholder="Province">
                    </div>
                    <div class="col-md-4">
                        <input type="text" class="form-control" name="superintendent_residental_premises_postal_code" placeholder="Postal Code">
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
