<div class="row">
    <div class="col-md-10 second_text_main">
        <p class="">Lessor Information</p>
        <span>One who rents real property or personal property to another</span>
    </div>
</div>

<form class="form-horizontal multiform">
    <div id="lessor_container">
        <div class="counter_lessor_elem">
            <div class="form-group">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-5">
                            <input type="text" class="form-control" name="lessor_info[0][name]" placeholder="Name">
                        </div>
                        <div class="col-md-2">
                            <select class="form-control" type="select" name="lessor_info[0][type]">
                                <option value="individual">Individual</option>
                                <option value="">1</option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <input type="text" class="form-control" name="lessor_info[0][phone]" placeholder="Phone No">
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-7">
                            <input type="text" class="form-control" name="lessor_info[0][address]" placeholder="Street Address and Street Name">
                        </div>
                        <div class="col-md-4">
                            <input type="text" class="form-control" name="lessor_info[0][apt]" placeholder="Apt, Site or Romm #">
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-5">
                            <input type="text" class="form-control" name="lessor_info[0][municipality]" placeholder="Municipality (or other)">
                        </div>
                        <div class="col-md-2">
                            <select class="form-control" type="select" name="lessor_info[0][province]">
                                <option value="province">Province</option>
                                <option value="">1</option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <input type="text" class="form-control" name="lessor_info[0][postal_code]" placeholder="Postal Code">
                        </div>
                    </div>
                </div>
            </div>
            <br>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-3 col-md-offset-8 text-right">
                <h4><a href="#" id="add_another_lessor"  class="add_another_lessor">Add Another Lessor</a></h4>
            </div>
        </div>
    </div>
</form>

<div class="col-md-10 second_text_main">
    <p class="">Lessee Information</p>
    <span>One who rents real property or personal property to another</span>
</div>

<form class="form-horizontal multiform">
    <div id="lessee_container">
        <div class="counter_lessee_elem">
            <div class="form-group">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <input type="text" class="form-control lessee_info_first_name" name="lessee_info[0][first_name]"  placeholder="First Name">
                        </div>
                        <div class="col-md-5">
                            <input type="text" class="form-control lessee_info_last_name" name="lessee_info[0][last_name]" placeholder="Last Name">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-3 col-md-offset-8 text-right">
                <h4><a href="#" class="add_another_lessee" id="add_another_lessee">Add Another Lessee</a></h4>
            </div>
        </div>
    </div>
    <br>
    <br>
    <br>


    <div class="form-group">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-6">
                    <input type="text" class="form-control" name="lessee_info_phone" placeholder="Phone">
                </div>
                <div class="col-md-5">
                    <input type="text" class="form-control" name="lessee_info_email" placeholder="Email">
                </div>
            </div>
        </div>
    </div>

</form>


<?php
$this->registerJs("
    $('#add_another_lessor').on('click', function(e){
        e.preventDefault();
        var prof = document.getElementById('lessor_container');
        var counter_lessor = $('.counter_lessor_elem').length;
        var div_lessor = document.createElement('div');
        div_lessor.className = 'counter_lessor_elem';
        var html = '<div class=\"counter_lessor_elem\">'+
            '<br>'+
            '<div class=\"form-group\">'+
                '<div class=\"row\">'+
                    '<div class=\"col-md-12\">'+
                        '<div class=\"col-md-5\">'+
                            '<input type=\"text\" class=\"form-control\" name=\"lessor_info[' +counter_lessor+ '][name]\" placeholder=\"Name\">'+
                        '</div>'+
                        '<div class=\"col-md-2\">'+
                            '<select class=\"form-control\" type=\"select\" name=\"lessor_info[' +counter_lessor+ '][type]\">'+
                                '<option value=\"individual\">Individual</option>'+
                                '<option value=\"\">1</option>'+
                            '</select>'+
                        '</div>'+
                        '<div class=\"col-md-4\">'+
                            '<input type=\"text\" class=\"form-control\" name=\"lessor_info[' +counter_lessor+ '][phone]\" placeholder=\"Phone No\">'+
                        '</div>'+
                    '</div>'+
                '</div>'+
            '</div>'+
            '<div class=\"form-group\">'+
                '<div class=\"row\">'+
                    '<div class=\"col-md-12\">'+
                        '<div class=\"col-md-7\">'+
                            '<input type=\"text\" class=\"form-control\" name=\"lessor_info[' +counter_lessor+ '][address]\" placeholder=\"Street Address and Street Name\">'+
                        '</div>'+
                        '<div class=\"col-md-4\">'+
                            '<input type=\"text\" class=\"form-control\" name=\"lessor_info[' +counter_lessor+ '][apt]\" placeholder=\"Apt, Site or Romm #\">'+
                        '</div>'+
                    '</div>'+
                '</div>'+
            '</div>'+
            '<div class=\"form-group\">'+
                '<div class=\"row\">'+
                    '<div class=\"col-md-12\">'+
                        '<div class=\"col-md-5\">'+
                            '<input type=\"text\" class=\"form-control\" name=\"lessor_info[' +counter_lessor+ '][municipality]\" placeholder=\"Municipality (or other)\">'+
                        '</div>'+
                        '<div class=\"col-md-2\">'+
                            '<select class=\"form-control\" type=\"select\" name=\"lessor_info[' +counter_lessor+ '][province]\">'+
                                '<option value=\"province\">Province</option>'+
                                '<option value=\"\">1</option>'+
                            '</select>'+
                        '</div>'+
                        '<div class=\"col-md-4\">'+
                            '<input type=\"text\" class=\"form-control\" name=\"lessor_info[' +counter_lessor+ '][postal_code]\" placeholder=\"Postal Code\">'+
                        '</div>'+
                    '</div>'+
                '</div>'+
            '</div>'+
            '<br>'+
        '</div>';

        div_lessor.innerHTML = html;
        prof.appendChild(div_lessor);
    });

    $('#add_another_lessee').on('click', function(e){
        e.preventDefault();
        var prof = document.getElementById('lessee_container');
        var counter_lessee = $('.counter_lessee_elem').length;
        var div_lessor = document.createElement('div');
        div_lessor.className = 'counter_lessor_elem';
        var html = '<div class=\"counter_lessee_elem\">'+
            '<br>'+
            '<div class=\"form-group\">'+
                '<div class=\"row\">'+
                    '<div class=\"col-md-12\">'+
                        '<div class=\"col-md-6\">'+
                            '<input type=\"text\" class=\"form-control lessee_info_first_name\" name=\"lessee_info[' +counter_lessee+ '][first_name]\"  placeholder=\"First Name\">'+
                        '</div>'+
                        '<div class=\"col-md-5\">'+
                            '<input type=\"text\" class=\"form-control lessee_info_last_name\" name=\"lessee_info[' +counter_lessee+ '][last_name]\" placeholder=\"Last Name\">'+
                        '</div>'+
                    '</div>'+
                '</div>'+
            '</div>'+
        '</div>';

        div_lessor.innerHTML = html;
        prof.appendChild(div_lessor);
    });

    ", yii\web\View::POS_READY);
?>