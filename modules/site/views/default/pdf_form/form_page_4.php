<div class="row">
    <div class="col-md-11">
        <p class="second_text_main">Length Tenancy</p>
    </div>
</div>

<form class="form-horizontal multiform">
    <div class="form-group">
        <div class="row">
            <div class='col-sm-6  col-md-offset-4'>
                <div class='col-sm-6'>
                    <div class="form-group">
                        <label style="line-height: 44px;">This tenancy starts on:</label>
                        <div class='input-group date' id='datetimepicker_start'>
                            <input type='text' name="length_tenancy_date_start" class="form-control" placeholder="              /               /"/>
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <br>

        <div class='col-sm-6  col-md-offset-4'>
            <div class='col-sm-6'>
                <div class="form-group">
                    <div class="radio">
                        <label>
                            <input type="radio" name="length_tenancy_period" value="Fixed term tennacy">
                            Fixed term tennacy
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="length_tenancy_period" value="Renews monthly">
                            Renews monthly
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="length_tenancy_period" value="Renews weekly">
                            Renews weekly
                        </label>
                    </div>
                </div>
            </div>
        </div>

        <br>
        <br>

        <div class="row">
            <div class='col-sm-6  col-md-offset-4'>
                <div class='col-sm-6'>
                    <div class="form-group">
                        <label style="line-height: 44px;">Fixed term tenancy ends on:</label>
                        <div class='input-group date' id='datetimepicker_end'>
                            <input type='text' name="length_tenancy_date_end" class="form-control" placeholder="              /               /"/>
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>