<div class="row">
    <div class="col-md-11">
        <p class="second_text_main">Rent</p>
    </div>
</div>
<div class="row">
    <div class="col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2">
        <form class="form-horizontal pay-area_rent-1 multiform">
            <div class="form-group form-group-lg">
                <label for="inputpay" class="col-sm-7 control-label">The lessee will pay</label>

                <div class="col-sm-5">
                    <input type="text" name="rent_lessee_will_pay" class="form-control" placeholder="$">
                </div>
            </div>
        </form>
    </div>
    <div class="col-md-3 col-sm-4 col-sm-offset-0">
        <form class="form-horizontal multiform">
            <div class="form-group form-group-lg">
                <label for="input" class="col-sm-3 control-label">every</label>
                <div class="col-sm-6">
                    <select class="form-control" id="rent_pay_period" type="select" name="rent_pay_period">
                        <option>month</option>
                        <option>week</option>
                    </select>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="row">
    <div class="col-md-6 col-sm-9 col-md-offset-2 col-sm-offset-1">
        <form class="form-horizontal multiform">
            <div class="form-group form-group-lg" id="rent_pay_point_info">
                <label for="dayofweek" class="col-md-4 col-sm-3 col-md-offset-1 col-sm-offset-2 control-label">Rent is due on the</label>
                <div class="col-md-3 col-sm-3">
                    <select class="form-control" type="select" name="rent_pay_point">
                        <?php for($i = 1; $i<=31; $i++ ){?>
                            <option><?=$i ?>st</option>
                        <?php } ?>
                    </select>
                </div>
                <label class="control-label">day of the month</label>
            </div>
        </form>
    </div>
</div>
<form class="multiform">
    <div class="row">
        <div class="form-group">
            <div class="col-sm-10 col-sm-offset-2 col-md-12 col-md-offset-0 to-lessor_rent-1">
                <div class="col-md-4 col-md-offset-3 col-sm-offset-1">
                    <span >Payments shall be delivered/mailed to lessor?</span>
                </div>
                <div class="col-md-1 col-md-offset-0 col-sm-2 col-sm-offset-3 col-xs-1 col-xs-offset-0">
                    <div class="checkbox"><label>
                            <input type="radio" name="payments_shall_be_delivered" value="Yes">
                            Yes</label>
                    </div>
                </div>
                <div class="col-md-1 col-md-offset-0 col-sm-2 col-sm-offset-1 col-xs-1 col-xs-offset-1">
                    <div class="checkbox"><label>
                            <input type="radio" name="payments_shall_be_delivered" value="No">
                            No</label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-12 addres_account-set">
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-11">
                        <input type="text" class="form-control" name="rent_lessor_name" placeholder="Name">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-7">
                        <input type="email" class="form-control streetadd_addres_account-set" name="rent_lessor_address" placeholder="Street Address and Street Name">
                    </div>

                    <div class="col-sm-4">
                        <input type="email" class="form-control apt_addres_account-set" name="rent_lessor_apt" placeholder="Apt. No">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-4">
                        <input type="text" class="form-control" name="rent_lessor_municipality" placeholder="Municipality (or other)">
                    </div>
                    <div class="col-sm-3">
                        <select class="form-control" type="select" name="rent_lessor_province">
                            <option value="Province" disabled selected hidden>Province</option>
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                        </select>
                    </div>
                    <div class="col-sm-4">
                        <input type="email" class="form-control" name="rent_lessor_postal_code" placeholder="Postal Code">
                    </div>

                </div>
            </div>
        </div>
    </div>
</form>

<?php
$this->registerJs("
    $('#rent_pay_period').on('change', function(e){
        var prof = document.getElementById('rent_pay_point_info');
        var html = '';
        if($(this).val() == 'week'){
            html = '<label for=\"dayofweek\" class=\"col-md-4 col-sm-3 col-md-offset-1 col-sm-offset-2 control-label\">Rent is due on the</label>'+
                '<div class=\"col-md-4 col-sm-4\">'+
                    '<select class=\"form-control\" type=\"select\" name=\"rent_pay_point\">'+
                        '<option>Monday</option>'+
                        '<option>Tuesday</option>'+
                        '<option>Wednesday</option>'+
                        '<option>Thursday</option>'+
                        '<option>Friday</option>'+
                        '<option>Saturday</option>'+
                        '<option>Sunday</option>'+
                    '</select>'+
                '</div>'+
                '<label class=\"control-label\">of the week</label>';
        } else {
            html = '<label for=\"dayofweek\" class=\"col-md-4 col-sm-3 col-md-offset-1 col-sm-offset-2 control-label\">Rent is due on the</label>'+
                '<div class=\"col-md-3 col-sm-3\">'+
                    '<select class=\"form-control\" type=\"select\" name=\"rent_pay_point\">';
                    for(var i = 1; i <= 31; i++){
                        html +='<option>' + i + 'st</option>';
                    }

                    html += '</select>'+
                '</div>'+
                '<label class=\"control-label\">day of the month</label>';
        }

        prof.innerHTML = html;
//        if(e.keyCode == 13){
//            var new_option = $(this).val();
//            console.log($(this).val());
//            $(this).val('');
//
//            var prof = document.getElementById('appliances_container');
//            var counter_appl_opt = $('.counter_appliances_options').length;
//            var div_appl_opt = document.createElement('div');
//            div_appl_opt.className = 'counter_appliances_options';
//            var html = '<div class=\"checkbox\">'+
//                            '<label>'+
//                                '<input type=\"checkbox\" name=\"my_appliances_options[' + new_option + ']\">' + new_option +
//                            '</label>'+
//                        '</div>';
//
//            div_appl_opt.innerHTML = html;
//            prof.appendChild(div_appl_opt);
//        }
    });

    ", yii\web\View::POS_READY);
?>