<div class="row">
    <div class="col-md-11">
        <p class="second_text_main">Will the tenant be charged extra for late payments?</p>
    </div>
</div>
<form class="multiform">
    <div class="row">
        <div class="col-sm-11 col-md-11">
            <div class="col-md-1 col-md-offset-5 col-sm-2 col-sm-offset-4 col-xs-1 col-xs-offset-4">
                <div class="radio"><label>
                        <input type="radio" name="extra_pay_for_late_payments" value="yes">
                        Yes</label>
                </div>
            </div>
            <div class="col-md-1 col-md-offset-0 col-sm-2 col-sm-offset-1 col-xs-1 col-xs-offset-1">
                <div class="radio"><label>
                    <input type="radio" name="extra_pay_for_late_payments" value="no">
                    No</label>
                </div>
            </div>
        </div>
    </div>
    <br>
    <br>
    <div class=" form-group col-md-11 text-center">
        <span>Late payment penalty shall not  exceed on per cent per month of the monthly rent.</span>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <div class="form-group form-group-lg">
                <div class="col-sm-3 col-md-offset-4 col-sm-offset-4">
                    <input class="form-control" type="text" name="late_payment_penalty" placeholder="$">
                </div>
            </div>
        </div>
    </div>
</form>