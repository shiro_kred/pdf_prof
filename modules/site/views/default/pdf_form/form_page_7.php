<form class="multiform">
    <div class="row">
        <div class="col-md-11">
            <p class="second_text_main">Will any security deposits be required?</p>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-11 col-md-11">
            <div class="col-md-1 col-md-offset-5 col-sm-2 col-sm-offset-4 col-xs-1 col-xs-offset-4">
                <div class="radio"><label>
                        <input type="radio" name="security_deposits_be_required" value="yes">
                        Yes</label>
                </div>
            </div>
            <div class="col-md-1 col-md-offset-0 col-sm-2 col-sm-offset-1 col-xs-1 col-xs-offset-1">
                <div class="radio"><label>
                    <input type="radio" name="security_deposits_be_required" value="no">
                    No</label>
                </div>
            </div>
        </div>
    </div>
    <br>
    <br>
    <div class="row form-horizontal">
        <div class="col-sm-11 form-group form-group-lg">
            <label class="control-label col-sm-4 col-md-offset-3">A security deposit in the amount of</label>
            <div class="col-sm-2">
                <input class="form-control" type="text" name="security_deposit_amount" placeholder="$">
            </div>
        </div>
    </div>

    <div class="row form-group">
        <div class="col-sm-11 col-md-11 text-center">
            <span>is to be paid by the lesse  to the lessor.(Not to exceed one week's</span>
        </div>
    </div>
    <div class="row form-group">
        <div class="col-sm-11 col-md-11 text-center">
            <span>rent under a weekly agreement: otherwise, one month's rent)</span>
        </div>
    </div>
</form>