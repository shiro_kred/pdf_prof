<form class="multiform">
    <div class="col-md-11">
        <p class="second_text_main">The rent include payments of the folowing services and facilities</p>
    </div>
    <div class="row">
        <div class="form-group-lg">
            <div class="col-md-11 col-sm-5 col-xs-5 col-md-offset-0 col-xs-offset-4">
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Appliances</label>
                        <div id="appliances_container">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="appliances_stove">Stove
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="appliances_refrigerator">Refrigerator
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="appliances_washer">Washer & Dryer
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="appliances_dishwasher">Dishwasher
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="appliances_furniture">Furniture
                                </label>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="">
                                <input type="text" class="form-control" id="add_my_option_appliances" placeholder="Add your own here">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-md-offset-0">
                    <div class="form-group">
                        <label>Utilites</label>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="utilites_washer">Washer & dryer (coin operated)
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="utilites_cable">Cable service
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="utilites_heat">Heat
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="utilites_water">Water
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="utilites_hot_water">Hot water
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="utilites_electricity">Electricity
                            </label>
                        </div>
                        <div class="checkbox form-group magic_input">
                            <label>
                                <input type="checkbox" name="utilites_parking">
                                <div class="">
                                    <span class="input_label">Parking: # of spaces</span>
                                    <div class="col-md-2 fix_div">
                                        <input type="text" name="utilites_parking_of_spaces" class="form-control">
                                    </div>
                                    <span class="input_label">space #</span>
                                    <div class="col-md-2 fix_div">
                                        <input type="text" name="utilites_parking_space" class="form-control">
                                    </div>
                                </div>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-md-offset-0">
                    <div class="form-group">
                        <label>Other</label>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="other_lawn_care">Lawn care
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="other_shawn_removal">Shawn removal
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="other_garbage_removal">Garbage removal
                            </label>
                        </div>
                        <div class="checkbox form-group magic_input">
                            <label>
                                <input type="checkbox" name="other_my_option_1">
                                <div class="float_left">
                                    <div class="col-md-12 col-sm-12 col-xs-12 fix_div">
                                        <input type="text" name="other_my_option_1_name" class="form-control">
                                    </div>
                                </div>
                            </label>
                        </div>
                        <div class="checkbox form-group magic_input">
                            <label>
                                <input type="checkbox" name="other_my_option_2">
                                <div class="float_left">
                                    <div class="col-md-12 col-sm-12 col-xs-12 fix_div">
                                        <input type="text" name="other_my_option_2_name" class="form-control">
                                    </div>
                                </div>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>

<?php
$this->registerJs("
    $('#add_my_option_appliances').keyup(function(e, pass){
        if(e.keyCode == 13 || pass){
            var new_option = $(this).val();
            $(this).val('');

            var prof = document.getElementById('appliances_container');
            var counter_appl_opt = $('.counter_appliances_options').length;
            var div_appl_opt = document.createElement('div');
            div_appl_opt.className = 'counter_appliances_options';
            var html = '<div class=\"checkbox\">'+
                            '<label>'+
                                '<input type=\"checkbox\" name=\"my_appliances_options[' + new_option + ']\" checked=\"checked\">' + new_option +
                            '</label>'+
                        '</div>';

            div_appl_opt.innerHTML = html;
            prof.appendChild(div_appl_opt);
        }
    });

    ", yii\web\View::POS_READY);
?>