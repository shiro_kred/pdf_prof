<form class="multiform">
    <div class="col-md-11">
        <p class="second_text_main">The folowing services and facilities are the responsibility of the Lessee</p>
    </div>
    <div class="row">
        <div class="col-md-11 col-sm-11 col-xs-11">
            <div class="col-md-7 col-md-offset-4 col-sm-offset-4 col-xs-offset-4">
                <div class="form-group">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="lessee_duties_lawn_care">Lawn care
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="lessee_duties_show_removal">Show removal
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="lessee_duties_garbage_removal">Garbage removal
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="lessee_duties_tenant_insurance">Tenant insurance
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="lessee_duties_separation_recyclables">Separation of recyclables, organics and refuse
                        </label>
                    </div>
                    <div class="checkbox form-group magic_input">
                        <label>
                            <input type="checkbox" name="lessee_duties_parking_option">
                            <div class="">
                                <span class="input_label">Parking @</span>
                                <div class="col-md-3 fix_div">
                                    <input type="text" name="lessee_duties_parking_text" class="form-control" placeholder="$">
                                </div>
                                <span class="input_label">/ month for</span>
                                <div class="col-md-2 fix_div">
                                    <input type="text" name="lessee_duties_parking_month" class="form-control">
                                </div>
                            </div>
                        </label>
                    </div>
                    <div class="checkbox form-group magic_input">
                        <label>
                            <input type="checkbox" name="lessee_duties_returned_exceed_option">
                            <div class="">
                                <span class="input_label">Returned cheque charges not to exceed</span>
                                <div class="col-md-3 fix_div">
                                    <input type="text" name="lessee_duties_returned_exceed_text" class="form-control" placeholder="$">
                                </div>
                            </div>
                        </label>
                    </div>
                    <div class="checkbox form-group magic_input">
                        <label>
                            <input type="checkbox" name="lessee_duties_locked_exceed_option">
                            <div class="">
                                <span class="input_label">Locked out charges/keys not to exceed</span>
                                <div class="col-md-3 fix_div">
                                    <input type="text" class="form-control" name="lessee_duties_locked_text" placeholder="$">
                                </div>
                            </div>
                        </label>
                    </div>
                    <div class="checkbox form-group magic_input">
                        <label>
                            <input type="checkbox" name="lessee_duties_my_option_1">
                            <div class="float_left">
                                <div class="col-md-12 col-sm-12 col-xs-12 fix_div">
                                    <input type="text" name="lessee_duties_my_option_1_text" class="form-control">
                                </div>
                            </div>
                        </label>
                    </div>
                    <div class="checkbox form-group magic_input">
                        <label>
                            <input type="checkbox" name="lessee_duties_my_option_2">
                            <div class="float_left">
                                <div class="col-md-12 col-sm-12 col-xs-12 fix_div">
                                    <input type="text" name="lessee_duties_my_option_2_text" class="form-control">
                                </div>
                            </div>
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>