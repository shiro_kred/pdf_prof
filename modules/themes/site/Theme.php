<?php

namespace root\modules\themes\site;

use Yii;

/**
 * Class Theme
 * @package root\modules\themes\admin
 */
class Theme extends \yii\base\Theme
{
    /**
     * @inheritdoc
     */
    public $pathMap = [
        '@frontend/views' => '@root/modules/themes/site/views',
        '@frontend/modules' => '@root/modules/themes/site/modules'
    ];

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        Yii::$app->assetManager->bundles['yii\bootstrap\BootstrapAsset'] = [
            'sourcePath' => '@root/modules/themes/site/assets',
            'css' => [
                'css/bootstrap.min.css'
            ]
        ];
        Yii::$app->assetManager->bundles['yii\bootstrap\BootstrapPluginAsset'] = [
            'sourcePath' => '@root/modules/themes/site/assets',
            'js' => [
                'js/bootstrap.min.js'
            ]
        ];
    }
}
