<?php

namespace root\modules\themes\site;

use yii\web\AssetBundle;

/**
 * Theme main asset bundle.
 */
class ThemeAsset extends AssetBundle
{
    /**
     * @inheritdoc
     */
    public $sourcePath = '@root/modules/themes/site/assets';

    /**
     * @inheritdoc
     */
    public $css = [
        'css/style.css',
    ];

    public $js = [
        'js/main.js',
        'js/jquery.nicescroll.js'
    ];

    /**
     * @inheritdoc
     */
    public $depends = [
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'yii\jui\JuiAsset'
    ];
}
