jQuery(function($) {
	$('#button_signup').on('click', function(e){
		e.preventDefault();
		signup();
	});

	$('#button_login').on('click', function(e){
		//if(window.location.pathname != '/'){
			e.preventDefault();
			login();
		//}
	});

	$('#save_second_menu').on('click', function(){
		$.ajax({
			url: '/users/default/is-guest/',
			type: 'post',
			data: [],
			success: function (response) {
				if(response.is_guest == true){
					showLoginForm();
				} else {
					sendForm();
				}
			}
		});
	});

	function getFormData(){
		var result = '';
		$('.multiform').each(function(){
			result = result + '&' +$(this).serialize();
		});

		return result;
	}

	function sendForm(){
		var csrfToken = $('meta[name="csrf-token"]').attr("content");
		var data = getFormData();
		$.ajax({
			url: '/users/user/save-pdf/',
			type: 'post',
			data: {data:data, _csrf:csrfToken},
			success: function (response) {
				if(response.success == true){
					openPage('/users/user/account/');
				} else {
					alert('Sorry, sended form have problem.');
				}
			}
		});
	}

	function signup(){
		var form = $('#form_signin').serialize();
		$.ajax({
			url: '/signup/',
			type: 'post',
			data: form,
			success: function (response) {
				if(response.success == true){
					if(isHomePage()){
						openPage('/users/user/account/');
					} else {
						sendForm();
					}
				} else {
					console.log(response);
					if(response['user-email'] != undefined){
						$('#form_signin .field-inputEmail').addClass('has-error');
						$('#form_signin .field-inputEmail .help-block').text(response['user-email']);
						$('.content_popup_signin').effect('bounce', {direction: 'left'});
					}
				}
			}
		});
	}

	function login(){
		var form = $('#form_login').serialize();
		$.ajax({
			url: '/login/',
			type: 'post',
			data: form,
			success: function (response) {
				if(response.success == true){
					if(isHomePage()){
						openPage('/users/user/account/');
					} else {
						sendForm();
					}
				} else {
					if(response['loginform-password'] != undefined){
						$('#form_login').addClass('has-error');
						$('#window_login').effect('bounce', {direction: 'left'});
					}
				}
			}
		});
	}

	$('#signup_link').on('click', function(e){
		e.preventDefault();
		showSignupForm();
	});

	$('#login_link').on('click', function(e){
		e.preventDefault();
		showLoginForm();
	});

	$('#overlay').on('click', function(e){
		e.preventDefault();
		hideForm();
	});

	$('#or_sign_up').on('click', function(e){
		e.preventDefault();
		hideForm();
		showSignupForm();
	});

	$('#or_login').on('click', function(e){
		e.preventDefault();
		hideForm();
		showLoginForm();
	});

	function showLoginForm(){
		showOverlay();
		document.getElementById('window_login').style.display = 'block';
	}

	function showSignupForm(){
		showOverlay();
		document.getElementById('window_signin').style.display = 'block';
	}

	function hideForm(){
		hideOverlay();
		document.getElementById('window_login').style.display = 'none';
		document.getElementById('window_signin').style.display = 'none';
	}

	function showOverlay(){
		document.getElementById('overlay').style.display = 'block';
	}

	function hideOverlay(){
		document.getElementById('overlay').style.display = 'none';
	}

	function isHomePage(){
		if(location.pathname == '/'){
			return true;
		}
		return false;
	}

	function openPage($url){
		location.href = $url;
	}
});