<?php

/**
 * Head layout.
 */

use root\modules\themes\site\ThemeAsset;
use yii\helpers\Html;
use yii\helpers\Url;

?>
<title><?= Html::encode($this->title); ?></title>
<?= Html::csrfMetaTags(); ?>
<?php $this->head();
ThemeAsset::register($this);

$this->registerMetaTag(
    [
        'charset' => Yii::$app->charset
    ]
);
$this->registerMetaTag(
    [
        'name' => 'viewport',
        'content' => 'width=device-width, initial-scale=1.0'
    ]
);
$this->registerLinkTag(
    [
        'rel' => 'canonical',
        'href' => Url::canonical()
    ]
);
?>

<style>
    @font-face {
        font-family: AFuturaOrto;
        src: url(<?= $this->assetManager->publish('@root/modules/themes/site/assets/fonts/6851.ttf')[1] ?>)
    }
    @font-face {
        font-family: AFuturaOrtoBold;
        src: url(<?= $this->assetManager->publish('@root/modules/themes/site/assets/fonts/6845.ttf')[1] ?>);
    }
</style>

