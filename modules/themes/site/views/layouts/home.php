<?php

/**
 * Theme home layout.
 *
 * @var \yii\web\View $this View
 * @var string $content Content
 */

use root\modules\themes\site\widgets\Alert;
use yii\helpers\Url;

?>
<?php $this->beginPage(); ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <?= $this->render('//layouts/head') ?>
    </head>
    <body>
    <?php $this->beginBody(); ?>

    <div class="first_menu">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-8" id="logo_first_menu">
                    <img src="<?= $this->assetManager->publish('@root/modules/themes/site/assets/images/LOGO.png')[1] ?>" alt="LOGO">
                </div>
                <?= $this->render('top-menu') ?>
            </div>
        </div>
        <div class="navbar navbar-inverse navbar-static-top" id="items_menu">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#responsive-menu" id="akkordeon_menu"></button>
            </div>
            <div class="collapse navbar-collapse" id="responsive-menu">
                <ul class="nav navbar-nav" id="nav_menu">
                    <li><a href="#">Item 1</a></li>
                    <li><a href="#">Item 2</a></li>
                    <li><a href="#">Item 3</a></li>
                    <li><a href="#">Item 4</a></li>
                </ul>
            </div>
        </div>
    </div>

    <?= Alert::widget(); ?>

    <?= $content ?>

    <?php $this->endBody(); ?>

    </body>
    </html>
<?php $this->endPage(); ?>