<?php

/**
 * Theme main layout.
 *
 * @var \yii\web\View $this View
 * @var string $content Content
 */

use root\modules\themes\site\widgets\Alert;
use yii\helpers\Url;

?>
<?php $this->beginPage(); ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <?= $this->render('//layouts/head') ?>
    </head>
    <body>
    <?php $this->beginBody(); ?>

    <div class="first_menu">
        <div class="row">
            <div class="col-md-8" id="logo_first_menu">
                <a href="<?= Url::home();?>">
                    <img src="<?= $this->assetManager->publish('@root/modules/themes/site/assets/images/LOGO.png')[1] ?>" alt="LOGO">
                </a>
            </div>
            <?= $this->render('top-menu') ?>
        </div>
    </div>

    <!--/header-->

    <?= Alert::widget(); ?>

    <?= $content ?>

    <!--/#error-->
    <div class="container-fluid" >
        <div class="row" id="footer">
            <div class="col-lg-16" id="copyright_footer"> © Copyright 2016 by WebAppName.com. All Rights Reserved. </div>
            <a href="#">Privacy Policy</a>
            <a href="#">Terms of Service</a>
        </div>
    </div>
    <!--/#footer-->

    <?php $this->endBody(); ?>
    </body>
    </html>
<?php $this->endPage(); ?>