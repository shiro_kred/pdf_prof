<?php
use root\modules\users\models\Profile;
use yii\helpers\Url;
?>

<div class=" col-sm-5 col-sm-offset-6" id="links_first_menu">
    <div class="text-right">
        <?php if(Yii::$app->user->isGuest):?>
            <a href="#" id="login_link">LOG IN</a>
            <span>|</span>
            <a href="#" id="signup_link" >SIGN UP</a>
            <a href="#"></a>
        <?php else:?>
            <a href="<?= Url::toRoute('/users/user/account');?>">
                <?=strtoupper(Profile::getNameUser(Yii::$app->user->id)); ?>
            </a>
            <span>|</span>
            <a href="<?= Url::toRoute('/users/user/settings');?>">SETTINGS</a>
            <span>|</span>
            <a href="#">HELP</a>
            <span>|</span>
            <a href="/logout/" class="login_menu">SIGN OUT</a>
        <?php endif?>
    </div>
</div>