<?php

namespace root\modules\users\controllers\frontend;

use vova07\fileapi\actions\UploadAction as FileAPIUpload;
use root\modules\users\models\frontend\Email;
use root\modules\users\models\User;
use root\modules\users\models\Pdf;
use root\modules\users\models\UserAddress;
use root\modules\users\models\frontend\PasswordForm;
use root\modules\users\models\Profile;
use root\modules\users\Module;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\widgets\ActiveForm;
use Yii;

/**
 * Frontend controller for authenticated users.
 */
class PdfController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ]
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'fileapi-upload' => [
                'class' => FileAPIUpload::className(),
                'path' => $this->module->avatarsTempPath
            ]
        ];
    }

    public function actionGetPdfDescriptionInfo(){
        Yii::$app->response->format = 'json';
        if( Yii::$app->request->post('id')) {
            $id = Yii::$app->request->post('id');
            $model = Pdf::find()->where(['id' => $id])->one();

            if(!empty($model)){
                return ['success' => true, 'data' => $model->description];
            }
        }

        return ['success' => false];
    }

    public function beforeAction($action) {
        $this->enableCsrfValidation = ($action->id !== "get-pdf-description-info");
        return parent::beforeAction($action);
    }
}
