<?php

namespace root\modules\users\controllers\frontend;

use vova07\fileapi\actions\UploadAction as FileAPIUpload;
use root\modules\users\models\frontend\Email;
use root\modules\users\models\User;
use root\modules\users\models\Pdf;
use root\modules\users\models\UserAddress;
use root\modules\users\models\frontend\PasswordForm;
use root\modules\users\models\Profile;
use root\modules\users\Module;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\widgets\ActiveForm;
use Yii;

/**
 * Frontend controller for authenticated users.
 */
class UserController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ]
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'fileapi-upload' => [
                'class' => FileAPIUpload::className(),
                'path' => $this->module->avatarsTempPath
            ]
        ];
    }

    /**
     * Log Out page.
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Profile updating page.
     */
    public function actionUpdate()
    {
        $model = Profile::findByUserId(Yii::$app->user->id);

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                if ($model->save(false)) {
                    Yii::$app->session->setFlash('success', Module::t('users', 'FRONTEND_FLASH_SUCCES_UPDATE'));
                } else {
                    Yii::$app->session->setFlash('danger', Module::t('users', 'FRONTEND_FLASH_FAIL_UPDATE'));
                }
                return $this->refresh();
            } elseif (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
        }

        return $this->render(
            'update',
            [
                'model' => $model
            ]
        );
    }

    /**
     * Profile updating page.
     */
    public function actionNewUpdate()
    {
        $modelProfile = Profile::findByUserId(Yii::$app->user->id);
        $modelEmail = new Email();
        $modelPasswordForm = new PasswordForm();

        return $this->render(
            'new-update',
            [
                'modelProfile' => $modelProfile,
                'modelEmail' => $modelEmail,
                'modelPasswordForm' => $modelPasswordForm
            ]
        );
    }

    /**
     * Profile updating page.
     */
    public function actionSettings()
    {
        $modelProfile = Profile::findByUserId(Yii::$app->user->id);
        $modelUser = User::find()->where(['id' => Yii::$app->user->id])->one();
        $modelEmail = new Email();
        $modelPasswordForm = new PasswordForm();
        $modelUserAddress = UserAddress::find()->where(['user_id' => Yii::$app->user->id])->one();
        if(empty($modelUserAddress)){
            $modelUserAddress =  new UserAddress();
        }

        if(Yii::$app->request->post()){
            if ($modelUserAddress->load(Yii::$app->request->post())) {
                if ($modelUserAddress->validate()) {
                    $modelUserAddress->user_id = Yii::$app->user->id;
                    if ($modelUserAddress->save()) {
                        Yii::$app->session->setFlash('success', Module::t('users', 'FRONTEND_FLASH_SUCCES_UPDATE'));
                    } else {
                        Yii::$app->session->setFlash('danger', Module::t('users', 'FRONTEND_FLASH_FAIL_UPDATE'));
                    }
                    return $this->refresh();
                } elseif (Yii::$app->request->isAjax) {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return ActiveForm::validate($modelUserAddress);
                }
            }

            if ($modelProfile->load(Yii::$app->request->post())) {
                if ($modelProfile->validate()) {
                    if ($modelProfile->save()) {
                        Yii::$app->session->setFlash('success', Module::t('users', 'FRONTEND_FLASH_SUCCES_UPDATE'));
                    } else {
                        Yii::$app->session->setFlash('danger', Module::t('users', 'FRONTEND_FLASH_FAIL_UPDATE'));
                    }
                    return $this->refresh();
                } elseif (Yii::$app->request->isAjax) {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return ActiveForm::validate($modelProfile);
                }
            }

            if ($modelEmail->load(Yii::$app->request->post())) {
                if ($modelEmail->validate()) {
                    $modelUser->email = $modelEmail->email;
                    if ($modelUser->save()) {
                        Yii::$app->session->setFlash('success', Module::t('users', 'FRONTEND_FLASH_SUCCES_UPDATE'));
                    } else {
                        Yii::$app->session->setFlash('danger', Module::t('users', 'FRONTEND_FLASH_FAIL_UPDATE'));
                    }
                    return $this->refresh();
                } elseif (Yii::$app->request->isAjax) {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return ActiveForm::validate($modelEmail);
                }
            }

            if ($modelPasswordForm->load(Yii::$app->request->post())) {
                if ($modelPasswordForm->validate()) {
                    if ($modelPasswordForm->password()) {
                        Yii::$app->session->setFlash(
                            'success',
                            Module::t('users', 'FRONTEND_FLASH_SUCCESS_PASSWORD_CHANGE')
                        );
                        return $this->refresh();
                    } else {
                        Yii::$app->session->setFlash('danger', Module::t('users', 'FRONTEND_FLASH_FAIL_PASSWORD_CHANGE'));
                        return $this->refresh();
                    }
                } elseif (Yii::$app->request->isAjax) {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return ActiveForm::validate($modelPasswordForm);
                }
            }
        }

        return $this->render(
            'settings',
            [
                'modelProfile' => $modelProfile,
                'modelEmail' => $modelEmail,
                'modelPasswordForm' => $modelPasswordForm,
                'modelUserAddress' => $modelUserAddress
            ]
        );
    }

    /**
     * Profile updating page.
     */
    public function actionAccount()
    {
        $models = Pdf::find()->where(['user_id' => Yii::$app->user->id])->all();
        return $this->render('account', ['models' => $models]);
    }

    public function actionSavePdf(){
        Yii::$app->response->format = 'json';
        if(!empty(Yii::$app->request->post())) {
            $post = Yii::$app->request->post();
            if(!empty($post['data'])){
                parse_str($post['data'], $data);
                $model = new Pdf();
                $model->description = serialize($data);
                $model->user_id = Yii::$app->user->id;

                if($model->save(false)){
                    return ['success' => true];
                }
            }
        }

        return ['success' => false];
    }
}
