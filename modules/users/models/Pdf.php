<?php

namespace root\modules\users\models;

use root\modules\users\Module;
use yii\db\ActiveRecord;
use Yii;

/**
 * Class User
 * @package @root\modules\users\models
 * User model.
 *
 * @property integer $id ID
 * @property string $username Username
 * @property string $email E-mail
 * @property string $password_hash Password hash
 * @property string $auth_key Authentication key
 * @property string $role Role
 * @property integer $status_id Status
 * @property integer $created_at Created time
 * @property integer $updated_at Updated time
 *
 * @property Profile $profile Profile
 */
class Pdf extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users_pdf';
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }



    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'created_at' => Module::t('users', 'ATTR_CREATED'),
            'updated_at' => Module::t('users', 'ATTR_UPDATED'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->created_at = Yii::$app->formatter->asTimestamp(time());
            } else {
                $this->updated_at = Yii::$app->formatter->asTimestamp(time());
            }
            return true;
        }
        return false;
    }

    public function afterFind()
    {
        parent::afterFind();
        $this->description = unserialize($this->description);
    }

    /**
     * @return User|null User profile
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
