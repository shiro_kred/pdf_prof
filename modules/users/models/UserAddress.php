<?php

namespace root\modules\users\models;

use root\modules\users\Module;
use yii\db\ActiveRecord;
use Yii;

/**
 * Class User
 * @package @root\modules\users\models
 * User model.
 *
 * @property Profile $profile Profile
 */
class UserAddress extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users_address';
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                ['name', 'type', 'phone', 'street', 'apt', 'municipality', 'province', 'postal_code', 'lessor', 'user_id'],
                'safe'
            ],

            // Unique
            [['user_id'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'created_at' => Module::t('users', 'ATTR_CREATED'),
            'updated_at' => Module::t('users', 'ATTR_UPDATED'),
            'name' => 'Name',
            'type' => 'Type',
            'phone' => 'Phone',
            'street' => 'Street',
            'apt' => 'Apt No.',
            'municipality' => 'Municipality(or other)',
            'province' => 'Province',
            'postal_code' => 'Postal Code',
            'lessor' => 'Lessor',
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->created_at = Yii::$app->formatter->asTimestamp(time());
            } else {
                $this->updated_at = Yii::$app->formatter->asTimestamp(time());
            }
            return true;
        }
        return false;
    }
}
