<?php

/**
 * User form view.
 *
 * @var \yii\web\View $this View
 * @var \yii\widgets\ActiveForm $form Form
 * @var \vova07\users\models\backend\User $model Model
 * @var \vova07\users\models\Profile $profile Profile
 * @var array $roleArray Roles array
 * @var array $statusArray Statuses array
 * @var \vova07\themes\admin\widgets\Box $box Box widget instance
 */

use vova07\fileapi\Widget;
use root\modules\users\Module;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
<?php $form = ActiveForm::begin(); ?>
<?php $box->beginBody(); ?>

    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($modelPdf, 'transaction') ?>
        </div>
        <div class="col-sm-6">
            <?=
            $form->field($modelPdf, 'status_paid')->dropDownList([
                '0' => 'Not Paid',
                '1' => 'Paid'
            ]) ?>
        </div>
    </div>
<!-- pdf description -->
    <hr>
    <?= $this->render('@root/modules/site/views/default/pdf_form/form_page_1') ?>
    <hr>
    <?= $this->render('@root/modules/site/views/default/pdf_form/form_page_2') ?>
    <hr>
    <?= $this->render('@root/modules/site/views/default/pdf_form/form_page_3') ?>
    <hr>
    <?= $this->render('@root/modules/site/views/default/pdf_form/form_page_4') ?>
    <hr>
    <?= $this->render('@root/modules/site/views/default/pdf_form/form_page_5') ?>
    <hr>
    <?= $this->render('@root/modules/site/views/default/pdf_form/form_page_6') ?>
    <hr>
    <?= $this->render('@root/modules/site/views/default/pdf_form/form_page_7') ?>
    <hr>
    <?= $this->render('@root/modules/site/views/default/pdf_form/form_page_8') ?>
    <hr>
    <?= $this->render('@root/modules/site/views/default/pdf_form/form_page_9') ?>
    <hr>
    <?= $this->render('@root/modules/site/views/default/pdf_form/form_page_10') ?>
    <hr>
    <?= $this->render('@root/modules/site/views/default/pdf_form/form_page_11') ?>
    <hr>
    <?= $this->render('@root/modules/site/views/default/pdf_form/form_page_12') ?>


<?php $box->endBody(); ?>
<?php $box->beginFooter(); ?>
<?= Html::submitButton(
    'Update',
    [
        'class' => 'btn btn-primary btn-large'
    ]
) ?>
<?php $box->endFooter(); ?>
<?php ActiveForm::end(); ?>
<style type="text/css">
    .input_label{
        float: left;
    }

    .magic_input{
        padding-top: 13px;
        height: 34px;
    }

    .magic_input input[type=text]{
        margin-top: -13px;
    }

    .magic_input .fix_div{
        padding-left: 10px;
        padding-right: 5px;
    }

    .magic_input .input_label{
        padding-right: 5px;
    }

    .icheckbox_minimal{
        float: left;
    }

    .float_left {
        float: left;
    }
</style>

<?php
$this->registerJs("
////setTimeout(setValues, 4000);
////    setValues();
//    function setValues(){
//        var data = " . json_encode($modelPdf->description) . ";
//        $.each(data, function(key, value){
//            if(typeof value == 'object'){
//                console.log(key, value);
//                $.each(value, function(subkey, subvalue){
////                    if(Number(subkey) > 0){
////
////                    }
//                    $.each(subvalue, function(sub_subkey, sub_subvalue){
//                        var elem = $('*[name=\"'+key+'['+subkey+']'+'['+sub_subkey+']'+'\"]');
//                        console.log(elem);
//                    });
////                    setValueInElem(elem, subkey, subvalue);
//                });
//            } else {
//                var elem = $('*[name='+key+']');
//                setValueInElem(elem, key, value);
//            }
//        });
//    }
//
//    function setValueInElem(elem, key, value){
//        if($(elem).attr('type') == 'text'){
//            $(elem).val(value);
//        }
//
//        if($(elem).attr('type') == 'checkbox' || $(elem).attr('type') == 'radio'){
//            $(elem).iCheck('check')
//        }
//    }

    ", yii\web\View::POS_READY);

$this->registerJsFile('@web/js/js.js', ['position' => yii\web\View::POS_END]);
?>

