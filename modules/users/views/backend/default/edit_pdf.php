<?php

/**
 * User create view.
 *
 * @var \yii\web\View $this View
 * @var \vova07\users\models\backend\User $user User
 * @var \vova07\users\models\Profile $profile Profile
 * @var array $roleArray Roles array
 * @var array $statusArray Statuses array
 * @var \vova07\themes\admin\widgets\Box $box Box widget instance
 */

use root\modules\themes\admin\widgets\Box;
use root\modules\users\Module;

$this->title = 'Edit';
$this->params['subtitle'] = 'PDF Edit';
$this->params['breadcrumbs'] = [
    [
        'label' => $this->title,
        'url' => ['index'],
    ],
    $this->params['subtitle']
]; ?>
<div id="pdf" data-id="<?= $modelPdf->id?>"></div>
<div class="row">
    <div class="col-sm-12">
        <?php $box = Box::begin(
            [
                'title' => $this->params['subtitle'],
                'renderBody' => false,
                'options' => [
                    'class' => 'box-primary'
                ],
                'bodyOptions' => [
                    'class' => 'table-responsive'
                ],
                'buttonsTemplate' => '{cancel}'
            ]
        );
        echo $this->render(
            '_form_pdf',
            [
                'modelPdf' => $modelPdf,
                'box' => $box
            ]
        );
        Box::end(); ?>
    </div>
</div>