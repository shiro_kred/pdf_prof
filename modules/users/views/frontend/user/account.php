<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12 undermenu_account">
            <span>My Account</span>
            <div class="line_account"></div>
        </div>
    </div>
</div>

<div class="container-fluid group_l-agr_account">
    <div class="row row-centered">
        <?php foreach($models as $model): ?>
            <div class="col-md-5 col-centered l-agr_account">
                <div class="header_l-agr_account">
                    <span>Lease Agreement</span>
                </div>
                <img src="<?= $this->assetManager->publish('@root/modules/themes/site/assets/images/account/files.png')[1] ?>">
                <ul class="left_l-agr_account">
                    <li>Created On:</li>
                    <li>Status:</li>
                    <li>Transaction No:</li>
                </ul>
                <ul class="notpaid_right_l-agr_account">
                    <li><?php echo Yii::$app->formatter->asDatetime($model->created_at, 'php:M d, Y') ?></li>
                    <li><?php echo($model->status_paid == 1)? 'Paid':'Not Paid' ?></li>
                    <li><?php echo($model->transaction == 0)? '-': '<span class="text_decor_underline">'.$model->transaction.'</span>' ?></li>
                </ul>
                <div class="notpaid_butt_account">
                    <button class="btn btn-default view" type="submit">View</button>
                    <button class="btn btn-default edit" type="submit">Edit</button>
                    <button class="btn btn-default pay_now" type="submit">Pay Now</button>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>