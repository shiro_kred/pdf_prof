<?php

/**
 * Update profile page view.
 *
 * @var \yii\web\View $this View
 * @var \yii\widgets\ActiveForm $form Form
 * @var \vova07\users\models\frontend\User $model Model
 */

use vova07\fileapi\Widget;
use root\modules\users\Module;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

$this->title = Module::t('users', 'FRONTEND_UPDATE_TITLE');
$this->params['breadcrumbs'] = [
    Module::t('users', 'FRONTEND_SETTINGS_LABEL'),
    $this->title
];
$this->params['contentId'] = 'error'; ?>
<?php $form = ActiveForm::begin(
    [
        'options' => [
            'class' => 'center'
        ],
        'action' => Url::toRoute(['update']),
    ]
); ?>
    <fieldset class="registration-form">
        <?= $form->field($modelProfile, 'name')->textInput(['placeholder' => $modelProfile->getAttributeLabel('name')])->label(
            false
        ) ?>
        <?= $form->field($modelProfile, 'surname')->textInput(['placeholder' => $modelProfile->getAttributeLabel('surname')])->label(
            false
        ) ?>
        <?=
        $form->field($modelProfile, 'avatar_url')->widget(
            Widget::className(),
            [
                'settings' => [
                    'url' => ['fileapi-upload']
                ],
                'crop' => true,
                'cropResizeWidth' => 100,
                'cropResizeHeight' => 100
            ]
        )->label(false) ?>
        <?= Html::submitButton(
            Module::t('users', 'FRONTEND_UPDATE_SUBMIT'),
            [
                'class' => 'btn btn-primary pull-right'
            ]
        ) ?>
    </fieldset>
<?php ActiveForm::end(); ?>


<?php $form = ActiveForm::begin(
    [
        'options' => [
            'class' => 'center'
        ],
        'action' => Url::toRoute(['email']),
    ]
); ?>
<fieldset class="registration-form">
    <?= $form->field($modelEmail, 'oldemail')->textInput(['readonly' => true, 'placeholder' => $modelEmail->getAttributeLabel('oldemail')])->label(false) ?>
    <?= $form->field($modelEmail, 'email')->textInput(['placeholder' => $modelEmail->getAttributeLabel('email')])->label(false) ?>
    <?=
    Html::submitButton(
        Module::t('users', 'FRONTEND_EMAIL_CHANGE_SUBMIT'),
        [
            'class' => 'btn btn-primary pull-right'
        ]
    ) ?>
</fieldset>
<?php ActiveForm::end(); ?>



<?php $form = ActiveForm::begin(
    [
        'options' => [
            'class' => 'center',
        ],
        'action' => Url::toRoute(['password']),
    ]
); ?>
<fieldset class="registration-form">
    <?= $form->field($modelPasswordForm, 'oldpassword')->passwordInput(['placeholder' => $modelPasswordForm->getAttributeLabel('oldpassword')])->label(false) ?>
    <?= $form->field($modelPasswordForm, 'password')->passwordInput(['placeholder' => $modelPasswordForm->getAttributeLabel('password')])->label(false) ?>
    <?= $form->field($modelPasswordForm, 'repassword')->passwordInput(['placeholder' => $modelPasswordForm->getAttributeLabel('repassword')])->label(false) ?>
    <?= Html::submitButton(
        Module::t('users', 'FRONTEND_PASSWORD_CHANGE_SUBMIT'),
        [
            'class' => 'btn btn-primary pull-right'
        ]
    ) ?>
</fieldset>
<?php ActiveForm::end(); ?>
