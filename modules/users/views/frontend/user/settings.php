<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<div class="container-fluid">
    <div class="row">
        <h1 class="title_account-set">Account Settings</h1>
        <div class="col-md-12 basic-inf_account-set">
            <h2 class="subtitle_account-set">My Basic Info</h2>
            <?php $form = ActiveForm::begin(
                [
                    'options' => [
                        'class' => 'form-horizontal'
                    ],

                    'fieldConfig' => [
                        'template' => '{label}<div class="col-sm-5">{input}{error}</div>',
                        'labelOptions' => ['class' => 'col-sm-2 control-label'],
                    ],
                ]
            ); ?>
            <fieldset class="registration-form">
                <?= $form->field($modelProfile, 'name')->textInput(['placeholder' => $modelProfile->getAttributeLabel('name')])?>
                <?= $form->field($modelProfile, 'surname')->textInput(['placeholder' => $modelProfile->getAttributeLabel('surname')])?>
                <div class="col-md-6 col-md-offset-2 container-butt_account-set">
                    <?= Html::submitButton(
                        'Save Changes',
                        [
                            'class' => 'btn btn-default save_account-set'
                        ]
                    ) ?>
                </div>
            </fieldset>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
    <div class="line_account-set"></div>
    <div class="row">
        <div class="col-xs-offset-1 col-sm-10 col-md-11 col-md-offset-1 addres_account-set">
            <h2 class="subtitle_account-set">Addres</h2>
            <?php $form = ActiveForm::begin(
                [

                    'fieldConfig' => [
                        'template' => '<div class="col-sm-4">{label}{input}{error}</div>',
                        'labelOptions' => ['class' => 'col-sm-2 control-label'],
                    ],
                ]
            ); ?>

                <?= $form->field($modelUserAddress, 'lessor', [
                    'template' => "{label}<div class='col-sm-11'>{input}{error}</div>"
                ])->checkBox();?>

                <?= $form->field($modelUserAddress, 'name')->textInput()
                    ->label($modelUserAddress->getAttributeLabel('name'),['class'=>''])
                ?>
                <?= $form->field($modelUserAddress, 'type', [
                    'template' => "<div class=\"col-sm-3\">{label}{input}{error}</div>"
                ])->dropDownList([1=>1,2=>2])
                    ->label($modelUserAddress->getAttributeLabel('type'),['class'=>''])
                ?>
                <?= $form->field($modelUserAddress, 'phone')->textInput()
                    ->label($modelUserAddress->getAttributeLabel('phone'),['class'=>''])
                ?>


                <?= $form->field($modelUserAddress, 'street', [
                    'template' => "<div class=\"col-sm-7\">{label}{input}{error}</div>"
                ])->textInput()
                    ->label($modelUserAddress->getAttributeLabel('street'),['class'=>''])
                ?>
                <?= $form->field($modelUserAddress, 'apt')->textInput()
                    ->label($modelUserAddress->getAttributeLabel('apt'),['class'=>''])
                ?>

                <?= $form->field($modelUserAddress, 'municipality')->textInput()
                    ->label($modelUserAddress->getAttributeLabel('municipality'),['class'=>''])
                ?>
                <?= $form->field($modelUserAddress, 'province', [
                    'template' => "<div class=\"col-sm-3\">{label}{input}{error}</div>"
                ])->dropDownList([1=>1,2=>2])
                    ->label($modelUserAddress->getAttributeLabel('province'),['class'=>''])
                ?>
                <?= $form->field($modelUserAddress, 'postal_code')->textInput()
                    ->label($modelUserAddress->getAttributeLabel('postal_code'),['class'=>''])
                ?>

                <div class="col-md-6 col-md-offset-2 container-butt_account-set">
                    <?= Html::submitButton(
                        'Save Changes',
                        [
                            'class' => 'btn btn-default save_account-set'
                        ]
                    ) ?>
                </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
    <div class="line_account-set"></div>
    <div class="row">
        <div class="col-md-12 basic-inf_account-set">
            <h2 class="subtitle_account-set">Change My Password</h2>
            <?php $form = ActiveForm::begin(
                [
                    'options' => [
                        'class' => 'form-horizontal'
                    ],

                    'fieldConfig' => [
                        'template' => '{label}<div class="col-sm-5">{input}{error}</div>',
                        'labelOptions' => ['class' => 'col-sm-2 control-label'],
                    ],
                ]
            ); ?>
            <fieldset class="registration-form">
                <?= $form->field($modelPasswordForm, 'oldpassword')->textInput()?>
                <?= $form->field($modelPasswordForm, 'password')->textInput()?>
                <?= $form->field($modelPasswordForm, 'repassword')->textInput()?>

                <div class="col-md-6 col-md-offset-2 container-butt_account-set">
                    <?= Html::submitButton(
                        'Change',
                        [
                            'class' => 'btn btn-default save_account-set'
                        ]
                    ) ?>
                </div>
            </fieldset>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
    <div class="line_account-set"></div>
    <div class="row">
        <div class="col-md-12 basic-inf_account-set">
            <h2 class="subtitle_account-set">Change My Email</h2>
            <?php $form = ActiveForm::begin(
                [
                    'options' => [
                        'class' => 'form-horizontal'
                    ],

                    'fieldConfig' => [
                        'template' => '{label}<div class="col-sm-5">{input}{error}</div>',
                        'labelOptions' => ['class' => 'col-sm-2 control-label'],
                    ],
                ]
            ); ?>
            <fieldset class="registration-form">
                <?= $form->field($modelEmail, 'oldemail')->textInput()?>
                <?= $form->field($modelEmail, 'email')->textInput()?>
                <div class="col-md-6 col-md-offset-2 container-butt_account-set">
                    <?= Html::submitButton(
                        'Change',
                        [
                            'class' => 'btn btn-default save_account-set'
                        ]
                    ) ?>
                </div>
            </fieldset>
            <?php ActiveForm::end(); ?>
        </div>
    </div>

</div>